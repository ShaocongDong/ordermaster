import pymysql

from flask import Flask
from flask_restful import Resource, Api
from flask_restful import reqparse
from flaskext.mysql import MySQL


app = Flask(__name__)
api = Api(app)

# MySQL configurations
app.config['MYSQL_DATABASE_USER'] = 'root'
app.config['MYSQL_DATABASE_PASSWORD'] = 'admin'
app.config['MYSQL_DATABASE_DB'] = 'ordermaster'
app.config['MYSQL_DATABASE_HOST'] = 'localhost'

mysql = MySQL()
mysql.init_app(app)
conn = mysql.connect()
cursor = conn.cursor()


class CreateOrder(Resource):
    def post(self):
        try:
            parser = reqparse.RequestParser()
            parser.add_argument('deskid', type=int, help="The desk ID customer seating at")
            parser.add_argument('dishid', type=int, help="The ID of the dish chosen")
            parser.add_argument('quantity', type=int, help="The quantity of the chosen dish")
            args = parser.parse_args()

            _deskId = int(args['deskid'])
            _dishId = int(args['dishid'])
            _quantity = int(args['quantity'])

            # save to the MySQL DB
            sql = "INSERT INTO orders(deskid, dishid, quantity) VALUES(%s, %s, %s)"
            data = (_deskId, _dishId, _quantity)
            cursor.execute(sql, data)
            conn.commit()
            return {'message': 'Success!', 'status': 200}
            # return {'deskid': _deskId, 'dishid': _dishId, 'quantity': _quantity}
        except Exception as e:
            return {'error': str(e)}


class ClearDesk(Resource):
    def post(self):
        try:
            parser = reqparse.RequestParser()
            parser.add_argument('deskid', type=int, help="The desk ID customer seating at")
            args = parser.parse_args()

            _deskId = int(args['deskid'])

            # alter MySQL DB
            sql = "DELETE FROM orders WHERE deskid=%s"
            data = (_deskId)
            cursor.execute(sql, data)
            conn.commit()
            return {'message': 'Success!', 'status': 200}
            # return {'deskid': _deskId, 'dishid': _dishId, 'quantity': _quantity}
        except Exception as e:
            return {'error': str(e)}


class GetDeskOrder(Resource):
    def get(self):
        try:
            parser = reqparse.RequestParser()
            parser.add_argument('deskid', type=int, help="The desk ID customer seating at")
            args = parser.parse_args()

            _deskId = int(args['deskid'])

            # alter MySQL DB
            sql = "SELECT * FROM orders WHERE deskid=%s"
            data = (_deskId)
            cursor.execute(sql, data)
            rv = cursor.fetchall()
            return {'result': str(rv), 'status': 200}
            # return {'deskid': _deskId, 'dishid': _dishId, 'quantity': _quantity}
        except Exception as e:
            return {'error': str(e)}


api.add_resource(CreateOrder, '/CreateOrder')
api.add_resource(ClearDesk, '/ClearDesk')
api.add_resource(GetDeskOrder, '/GetDeskOrder')

if __name__ == '__main__':
    app.run(debug=True)