# OrderMaster

A simple Restful API implemented with Python Flask & MySQL relational DB.


**Developer Note:**
* Run the server from the root directory: 

        ```python api.py```

* Three API endpoints currently exposed:
    1. Create an order
    2. Clear all orders from specific desk
    3. Get all orders from specific desk
* SQL (*.spf) script can be found in the root directory whereby the DB table relations can be generated accordingly.